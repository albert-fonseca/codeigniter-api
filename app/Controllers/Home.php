<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $headers = [
            'Authorization: Basic ZmFnbmVyLnNhbnRhbmE6TTNzdHJl',
            'Content-Type : application/json'
        ];
        // $url = 'http://wscrmsenai.fieb.org.br/Contrato.asmx/GetContratosGeral';
        // $url =  'http://senaiweb.fieb.org.br/FrameHTML/rm/api/TOTVSCustomizacao/ConsultasSQL/ExecutaConsultaSQL?codColigada=1&codSentenca=PORTCLIENTE.001&codSistema=S&parameters=00900716000145';
        // $ch = curl_init($url);

        $curl = curl_init();

        curl_setopt_array($curl ,[
            CURLOPT_URL => "http://senaiweb.fieb.org.br/FrameHTML/rm/api/TOTVSCustomizacao/ConsultasSQL/ExecutaConsultaSQL?codColigada=1&codSentenca=PORTCLIENTE.001&codSistema=S&parameters=00900716000145",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
        ]);


        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt_array($ch,[
        //     CURLOPT_HTTPHEADER => $headers,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_RETURNTRANSFER => true
        // ]);

        // $result = curl_exec($ch);
        $result = curl_exec($curl);

        // curl_close($ch);
        curl_close($curl);

        $data = json_decode($result);

        echo "<pre>";
            print_r($data);
        echo "</pre>";

        // return view('welcome_message',$data);
    }
}
